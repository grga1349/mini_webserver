#ifndef SOCKET_H
#define SOCKET_H

#include <iostream>
#include <winsock2.h>


class Socket
{
public:
    // wsa data
    WSADATA WsaDat;
    // main socket
    SOCKET mainSocket;
    // temp. socket
    SOCKET tempSocket[500];
    // server info struct
    SOCKADDR_IN serverInf;
    // i mode (unblocking the socket)
    u_long iMode;
    // constructor
    Socket();
    // destructor
    ~Socket();
};

#endif // SOCKET_H

#include "socket.h"


Socket::Socket()
{
    // winsock setup
    if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
    std::cout<<"Error: Winsock startup failed!"<<std::endl;
    else
    std::cout<<"Server Log: Winsock startup."<<std::endl;

    // socket setup
    mainSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    if(mainSocket==INVALID_SOCKET)
    std::cout<<"Error: Socket creation failed!"<<std::endl;
    else
    std::cout<<"Server Log: Main socket created."<<std::endl;

    // server setup
    serverInf.sin_family=AF_INET;
    serverInf.sin_addr.s_addr=INADDR_ANY;
    serverInf.sin_port=htons(80);

    // bind server setup with socket
    if(bind(mainSocket,(SOCKADDR*)(&serverInf),sizeof(serverInf))==SOCKET_ERROR)
    std::cout<<"Error: Unable to bind main socket to server info!"<<std::endl;
    else
    std::cout<<"Server Log: Main socket binded to server info."<<std::endl;

    //listen
    listen(mainSocket,1);

    // set i mode to 1
    iMode=1;
}

Socket::~Socket()
{
    // shutdown socket
    shutdown(mainSocket,SD_SEND);

    // close socket
    closesocket(mainSocket);

    // cleanup winsock
    WSACleanup();
}

